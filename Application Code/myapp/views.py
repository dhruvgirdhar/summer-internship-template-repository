from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
#import csrf from CSRF protection
from django.views.decorators.csrf import csrf_exempt
#import df_library
from library.df_response_lib import *
#import json to get json request
import json
import sqlite3
from flask import Flask,jsonify,make_response,request
from flask_restful import Api

def home(request):
    return render(request,"service.html")

def chatbot(request):
    return render(request,"chatbot.html")

@csrf_exempt
def webhook(request):
    # build request object
    req = json.loads(request.body)
    #get action from json
    action = req.get('queryResult').get('action')
    oid = req.get('queryResult').get('parameters').get('OrderID')
    # prepare response for suggestion chips
    aog = actions_on_google_response()

    if action == 'order_id123':

        aog_sc = aog.suggestion_chips([f"Current Status : {oid} ", f"Complete Order History : {oid}"])
        ff_response = fulfillment_response()
        ff_text = ff_response.fulfillment_text("Ok")
        ff_messages = ff_response.fulfillment_messages([aog_sc])
        reply = ff_response.main_response(ff_text, ff_messages)

    if action == 'current_status':

        items = find_order(oid)
        aog_sc = aog.basic_card('', subtitle="", formattedText=f"{items}", image=None, buttons=None)
        ff_response = fulfillment_response()
        ff_text = ff_response.fulfillment_text("Ok")
        ff_messages = ff_response.fulfillment_messages([aog_sc])
        reply = ff_response.main_response(ff_text, ff_messages)


    if action == 'complete_order_history':

        aog_sc=[]
        items2 = returnOrderStatus('OID101',0)
        for items in items2:
            aog_sc.append(aog.basic_card('', subtitle="", formattedText=f"{items}", image=None, buttons=None))

        # aog_list = aog.list_select("this is a list", list_elements)
        ff_response = fulfillment_response()
        ff_text = ff_response.fulfillment_text("ok")
        ff_messages = ff_response.fulfillment_messages(aog_sc)
        reply = ff_response.main_response(ff_text, ff_messages)
    # return generated response
    return JsonResponse(reply, safe=False)

def returnOrderStatus(oid,single):

    connection = sqlite3.connect('data3.db')

    cursor = connection.cursor()

    query = "SELECT * from history WHERE OrderID=?"
    result = cursor.execute(query,(oid,))

    items2=[]
    for result2 in result:
        items2.append(f" date: {result2[1]}  position: {result2[2]}  reason: {result2[3]}  location: {result2[4]}  status : {result2[5]}")

    connection.close()
    return items2

def find_order(oid):

    connection = sqlite3.connect('data3.db')

    cursor = connection.cursor()

    query = "SELECT * from orderStatus WHERE OrderID=?"
    result = cursor.execute(query,(oid,))

    items2=[]
    for result2 in result:
        connection.close()
        return ({'OrderID':result2[0],
                'expectedDate' : result2[1],
                'date': result2[2],
                'position': result2[3],
                'reason': result2[4],
                'delay': result2[5],
                'location': result2[6],
                'status' : result2[7]
        })
